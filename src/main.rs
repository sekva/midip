extern crate portaudio;
extern crate vst;

mod midi;
mod osc;

use std::env;

fn nota_freq(nota: f32) -> f32 {
    440.0 * (2.0_f32.powf(1.0 / 12.0)).powf(nota as f32)
}

fn main() {
    let mut args = env::args();
    args.next();
    let arquivo = &args.next().unwrap();
    println!("{}", arquivo);
    let mut midi = midi::MIDI::novo(arquivo).unwrap();
    midi.tt();

    let mut o = osc::Oscilador::novo();
    o.setar_frequencia(0.0);
    o.setar_volume(0.5);

    loop {
        if let Some(dte) = midi.prox() {
            let (dt, e) = dte;
            std::thread::sleep(std::time::Duration::from_micros((dt * 2000).into()));

            match e {
                midi::Evento::Midi(m) => {
                    let (nota, vol) = m.som();
                    o.setar_frequencia(nota_freq(nota as f32 - 69.0));
                    o.setar_volume(vol as f32 / 254 as f32);
                }

                _ => {}
            }
        } else {
            break;
        }
    }

    println!("opa");
}
